package com.tinysand.search.repository.elastic;

import com.tinysand.search.entity.elastic.ElasticArticle;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * @author tiny
 */
public interface ElasticArticleRepository
        extends ElasticsearchRepository<ElasticArticle, Integer> {
}
