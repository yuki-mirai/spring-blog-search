package com.tinysand.search.repository.mysql;

import com.tinysand.search.entity.mysql.MysqlArticle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author tiny
 */
public interface MysqlArticleRepository
        extends JpaRepository<MysqlArticle, Integer> {

    // org.springframework.data.jpa.repository.Query
    @Query("select e from MysqlArticle e order by e.createTime desc")
    List<MysqlArticle> queryAllMysqlArticles();

    @Query("select e from MysqlArticle e where e.title " +
            "like concat('%', :keyword, '%') or e.content " +
            "like concat('%', :keyword, '%')")
    List<MysqlArticle> queryMysqlArticlesByKeywords();
}
