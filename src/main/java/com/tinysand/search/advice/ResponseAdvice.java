package com.tinysand.search.advice;

import com.tinysand.search.annotation.IgnoreAdvice;
import com.tinysand.search.support.ResponseData;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.util.Objects;

/**
 * @author tiny
 */
@RestControllerAdvice
public class ResponseAdvice implements ResponseBodyAdvice<Object> {
    @Override
    public boolean supports(
            MethodParameter methodParameter,
            Class<? extends HttpMessageConverter<?>> aClass) {
        // 方法类上存在IgnoreAdvice注解，不需要增强处理
        if (methodParameter.getDeclaringClass().isAnnotationPresent(
                IgnoreAdvice.class)) {
            return false;
        }
        // 如果方法上存在IgnoreAdvice注解，则不需要增强处理
        return !Objects.requireNonNull(methodParameter.getMethod())
                .isAnnotationPresent(IgnoreAdvice.class
        );
    }

    @Override
    public Object beforeBodyWrite(
            Object o, MethodParameter methodParameter,
            MediaType mediaType,
            Class<? extends HttpMessageConverter<?>> aClass,
            ServerHttpRequest serverHttpRequest,
            ServerHttpResponse serverHttpResponse) {
        if (o == null) {
            return new ResponseData<>(0, "请求结果为空");
        } else if (o instanceof ResponseData) {
            return o;
        } else { return new ResponseData<>(o); } // 包装结果
    }
}
