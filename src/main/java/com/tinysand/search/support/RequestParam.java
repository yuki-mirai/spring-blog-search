package com.tinysand.search.support;

import lombok.Data;

/**
 * @author tiny
 */
@Data
public class RequestParam {
    private String keyword;
}
