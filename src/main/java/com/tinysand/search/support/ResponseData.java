package com.tinysand.search.support;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author tiny
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseData<T> implements Serializable {
    private Integer code;      // 请求响应码
    private String message;    // 请求响应消息
    private T result;          // 响应数据

    public ResponseData(T result) {
        this.code = 0;
        this.message = "请求处理成功";
        this.result = result;
    }

    public ResponseData(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
