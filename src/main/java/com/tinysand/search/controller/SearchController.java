package com.tinysand.search.controller;

import com.tinysand.search.entity.elastic.ElasticArticle;
import com.tinysand.search.entity.mysql.MysqlArticle;
import com.tinysand.search.repository.elastic.ElasticArticleRepository;
import com.tinysand.search.repository.mysql.MysqlArticleRepository;
import com.tinysand.search.support.RequestParam;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.data.domain.Page;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author tiny
 */
@RestController
@RequestMapping("/article")
public class SearchController {
    private final MysqlArticleRepository mysqlRepository;

    private final ElasticArticleRepository elasticRepository;

    public SearchController(MysqlArticleRepository mysqlRepository,
                            ElasticArticleRepository elasticRepository) {
        this.mysqlRepository = mysqlRepository;
        this.elasticRepository = elasticRepository;
    }

    @GetMapping
    public List<MysqlArticle> queryAllArticles() {
        return mysqlRepository.queryAllMysqlArticles();
    }

    @PostMapping
//    @IgnoreAdvice
    public List<ElasticArticle> handleSearchRequest(
            @RequestBody RequestParam requestParam) {
        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
        // 构造ES查询条件
        String keyword = requestParam.getKeyword();
        queryBuilder.should(QueryBuilders.matchPhraseQuery("title", keyword))
                .should(QueryBuilders.matchPhraseQuery("content", keyword));

        StopWatch watch = new StopWatch();
        watch.start(); // 开始计时

        Page<ElasticArticle> articles = (Page<ElasticArticle>)
                elasticRepository.search(queryBuilder);  // 检索数据

        watch.stop();  // 结束计时
        System.out.println(String.format("数据检索耗时：%s ms",
                watch.getTotalTimeMillis()));

        return articles.getContent();
    }

    @GetMapping("{id:\\d+}")
    public Object getArticleDetails(@PathVariable("id") Integer id) {
        return mysqlRepository.findById(id).orElseGet(MysqlArticle::new);
    }
}
