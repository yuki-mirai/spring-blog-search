package com.tinysand.search.controller;

import com.tinysand.search.repository.mysql.MysqlArticleRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author tiny
 */
@Controller
public class IndexController {

    private final MysqlArticleRepository mysqlRepository;

    public IndexController(MysqlArticleRepository mysqlRepository) {
        this.mysqlRepository = mysqlRepository;
    }

    @GetMapping("/")
    public String toIndexPage() {
        mysqlRepository.findAll();
        return "index.html";
    }
}
