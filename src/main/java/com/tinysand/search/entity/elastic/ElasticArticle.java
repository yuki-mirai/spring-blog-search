package com.tinysand.search.entity.elastic;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Date;

/**
 * @author tiny
 */
@Data
// 指定实体类对应ES的索引名称为article，类型type是文档类型，使用服务器远程配置
// 为避免每次重启项目都将ES中的数据删除后再同步，createIndex指定为false
@Document(indexName = "article", type = "_doc",
        useServerConfiguration = true, createIndex = false)
public class ElasticArticle {
    @Id // org.springframework.data.annotation.Id
    private Integer id;          // 文章ID

    // 指定字段对应的ES类型是Text，analyzer指定分词器为ik_max_word
    @Field(type = FieldType.Text, analyzer = "ik_max_word")
    private String author;       // 文章作者

    @Field(type = FieldType.Text, analyzer = "ik_max_word")
    private String title;        // 文章标题

    @Field(type = FieldType.Text, analyzer = "ik_max_word")
    private String content;      // 文章正文内容

    // 指定字段对应ES中的类型是Date，使用自定义的日期格式化，pattern指定格式化
    // 规则是“日期时间”或“日期”或“时间毫秒”
    @Field(type = FieldType.Date, format = DateFormat.basic_date_time,
            pattern = "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis")
    private Date createTime;     // 文章创建时间

    @Field(type = FieldType.Date, format = DateFormat.custom,
            pattern = "yyyy-MM-ddHH:mm:ss||yyyy-MM-dd||epoch_millis")
    private Date updateTime;     // 文章更新时间
}
