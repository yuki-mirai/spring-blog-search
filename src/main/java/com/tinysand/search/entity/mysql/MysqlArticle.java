package com.tinysand.search.entity.mysql;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * @author tiny
 */
@Data
@Table(name = "t_article")       // 指定实体类对应的数据表
@Entity
public class MysqlArticle {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // 使用数据表定义的增长策略（自增）
    private Integer id;          // 文章ID

    private String author;       // 文章作者

    private String title;        // 文章标题

    // String类型默认映射的是varchar类型，content字段使用到了mediumtext，需要显式指定一下
    @Column(name = "content", columnDefinition = "mediumtext")
    private String content;      // 文章正文内容

    private Date createTime;     // 文章创建时间

    private Date updateTime;     // 文章更新时间
}