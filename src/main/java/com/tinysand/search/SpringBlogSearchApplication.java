package com.tinysand.search;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBlogSearchApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBlogSearchApplication.class, args);
    }

}
