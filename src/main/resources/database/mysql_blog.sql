-- 创建数据库
DROP DATABASE IF EXISTS `es_blog`;
CREATE DATABASE `es_blog`;

-- 创建博客（文章）表
CREATE TABLE `t_article`
(
    `id`          int(11)     NOT NULL AUTO_INCREMENT COMMENT '博客表自增ID',
    `title`       varchar(60) NOT NULL DEFAULT '' COMMENT '博客标题',
    `author`      varchar(60) NOT NULL DEFAULT '' COMMENT '博客作者',
    `content`     mediumtext  NOT NULL DEFAULT '' COMMENT '博客内容',
    `create_time` datetime    NOT NULL DEFAULT NOW() COMMENT '创建时间',
    `update_time` datetime    NOT NULL DEFAULT NOW() COMMENT '更新时间',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 DEFAULT charset = utf8mb4;
