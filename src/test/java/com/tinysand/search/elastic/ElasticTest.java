package com.tinysand.search.elastic;

import com.tinysand.search.repository.elastic.ElasticArticleRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author tiny
 */
@SpringBootTest
public class ElasticTest {
    @Autowired
    private ElasticArticleRepository elasticArticleRepository;

    @Test
    void testElasticSearch() {
        elasticArticleRepository.findAll().iterator()
                .forEachRemaining(elasticArticle ->
                        System.out.println(elasticArticle.getTitle()));
    }
}
